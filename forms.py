from flask_wtf import Form, validators
from wtforms.validators import Required
from wtforms import TextField, BooleanField, PasswordField
from .database import User
from . import helpers


class RegisterForm(Form):
    email = TextField(
        "email", [Required(message="Please provide a valid email address")]
    )
    password = PasswordField(
        "password", [Required(message="Please provide a valid password")]
    )
    mobile_number = TextField(
        "mobile_number", [Required(message="Please provide a valid mobile number")]
    )

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        if helpers.check_if_user_exists_by_email(self.email.data):
            self.email.errors.append("A user with the same email id already exists")
            return False

        if helpers.check_if_user_exists_by_mobile(self.mobile_number.data):
            self.mobile_number.append("A user with the mobile number already exists")
            return False
        return True


class LoginForm(Form):
    email = TextField(
        "email", [Required(message="Please provide a valid email address")]
    )
    password = PasswordField(
        "password", [Required(message="Please provide a valid password")]
    )

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            return False

        user = helpers.check_if_user_exists_by_email(self.email.data)
        if user is None:
            self.email.errors.append("Incorrect email or password")
            return False

        if not user.check_password(self.password.data):
            self.password.errors.append("Incorrect email or password")
            return False

        self.user = user
        return True