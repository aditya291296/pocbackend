from flask import (
    Flask,
    request,
    session,
    render_template,
    flash,
    url_for,
    redirect,
    jsonify,
    abort,
)
from .database import database, User
from flask_login import LoginManager
from flask_cors import cross_origin, CORS
from flask_jwt_extended import JWTManager
from flask_restful import abort, Api, Resource
from .Views import user_views
from .Views import organization_views

app = Flask(__name__, static_url_path="")
app.url_map.strict_slashes = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgres://postgres:postgres@localhost:5432/poc_dev"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["WTF_CSRF_ENABLED"] = True
app.config["SECRET_KEY"] = "a87b3d499bb713d4ad34e0b512fa7aea32dbf378e2353d7a"
app.config["CORS_HEADERS"] = "Content-Type"
login_manager = LoginManager(app)
database.init_app(app)
CORS(app)

login_manager.login_view = ".login"
login_manager.login_message = "You must sign in first to continue."
jwt = JWTManager(app)
api = Api(app)


@app.route("/")
def home():
    return ""


api.add_resource(user_views.RegisterAPIView, "/user/register/")
api.add_resource(user_views.LoginAPIView, "/user/login/")
api.add_resource(user_views.GmailLoginAPIView, "/user/login/gmail/")
api.add_resource(user_views.VerifyEmailView, "/verify/email/")
api.add_resource(
    user_views.UserSaveOnboardingDetailsAPIView, "/user/onboarding_details/save/"
)
api.add_resource(user_views.UserRetrieveAPIView, "/user/details/get/<string:user_id>/")
api.add_resource(
    organization_views.OrganizationCreateAPIView, "/user/organization/add/"
)
api.add_resource(
    organization_views.OrganizationDeleteAPIView,
    "/user/organization/delete/<string:organization_configuration_id>/<string:user_id>/<string:organization_id>/",
)
# Command Line
@app.cli.command()
@app.before_first_request
def initdb():
    database.create_all()


@app.cli.command()
def flushdb():
    database.drop_all()
    database.create_all()