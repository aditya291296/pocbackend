from flask_restful import reqparse, abort, Api, Resource
from .. import helpers
from flask_login import login_user
from flask_jwt_extended import create_access_token
import datetime
from ..database import database

parser = reqparse.RequestParser()


class RegisterAPIView(Resource):
    def post(self):
        parser.add_argument("email", type=str)
        parser.add_argument("password", type=str)
        args = parser.parse_args()
        if helpers.if_email_already_exists(args["email"]):
            return helpers.getInvalidResponse(
                "User with the given email id already exists"
            )
        created_user = helpers.create_user(args)
        helpers.send_verification_email(created_user.email)
        return helpers.getValidResponse({"email": created_user.email}, 201)


class LoginAPIView(Resource):
    def post(self):
        parser.add_argument("email", type=str)
        parser.add_argument("plainTextPassword", type=str)
        args = parser.parse_args()
        fetched_user = helpers.get_user_by_email(args["email"])
        if (
            fetched_user is not None
            and fetched_user.is_email_confirmed
            and fetched_user.check_password(args["plainTextPassword"])
        ):
            auth_token = create_access_token(
                identity=str(fetched_user.id), expires_delta=datetime.timedelta(days=7)
            )  # might change the time_delta as per the requirements.
            login_user(fetched_user, remember=True)
            organizations = helpers.get_user_organizations_details(fetched_user.id)
            return helpers.getValidResponse(
                {
                    "accessToken": auth_token,
                    "user": helpers.get_user_details_response(
                        organizations, fetched_user
                    ),
                },
                200,
            )
        else:
            return helpers.getInvalidResponse("Invalid Credentials")


class GmailLoginAPIView(Resource):
    def post(self):
        parser.add_argument("email", type=str)
        parser.add_argument("image_url", type=str)
        parser.add_argument("password", type=str)
        parser.add_argument("google_id", type=str)

        args = parser.parse_args()
        if helpers.check_if_user_exists_by_email(args["email"]):
            """ Just login User"""
            fetched_user = helpers.get_user_by_email(args["email"])
            auth_token = create_access_token(
                identity=str(fetched_user.id), expires_delta=datetime.timedelta(days=7)
            )
            login_user(fetched_user, remember=True)
            return helpers.getValidResponse(
                {"accessToken": auth_token, "user": {"id": fetched_user.id}}, 200
            )
        else:
            """ Register User and then login"""
            created_user = helpers.create_user_via_gmail_response(args)
            auth_token = create_access_token(
                identity=str(created_user.id), expires_delta=datetime.timedelta(days=7)
            )
            login_user(created_user, remember=True)
            return helpers.getValidResponse(
                {"accessToken": auth_token, "user": {"id": created_user.id}}, 200
            )


class VerifyEmailView(Resource):
    def get(self):
        parser.add_argument("key", type=str)
        parser.add_argument("value", type=str)
        args = parser.parse_args()
        fetched_record = helpers.get_key(args["key"])
        if fetched_record is not None and fetched_record.email == args["value"]:
            helpers.set_user_email_confirmed_flag(
                helpers.get_user_by_email(args["value"])
            )
            helpers.remove_value_from_cache(fetched_record)
            return helpers.getValidResponse(
                "Your email is verified now. You can close this tab and login", 200
            )
        else:
            return helpers.getInvalidResponse("Key does not exist ")


class UserSaveOnboardingDetailsAPIView(Resource):
    def put(self):
        parser.add_argument("domain_name", type=str)
        parser.add_argument("selected_plan", type=str)
        parser.add_argument("organization_name", type=str)
        parser.add_argument("user_id", type=str)
        args = parser.parse_args()
        fetched_user = helpers.get_user_by_id(args["user_id"])
        if fetched_user is not None:
            organization = helpers.create_organization(args)
            organization_configuration = helpers.create_organization_configuration(args)
            user_organization_relationship = helpers.create_user_org_relationships(
                fetched_user.id, organization.id, organization_configuration.id
            )
            fetched_user.are_onboarding_details_submitted = True
            fetched_user.organization_configuration_id = organization_configuration.id
            database.session.commit()

            return helpers.getValidResponse(
                "Successfully saved onboarding details for the given user !", 200
            )
        else:
            return helpers.getInvalidResponse(
                "user with the given email id does not exist "
            )


class UserRetrieveAPIView(Resource):
    def get(self, user_id):
        fetched_user = helpers.get_user_by_id(user_id)
        if fetched_user is not None:
            organizations = helpers.get_user_organizations_details(fetched_user.id)
            return helpers.getValidResponse(
                helpers.get_user_details_response(organizations, fetched_user),
                200,
            )
        return helpers.getInvalidResponse("User with the given id does not ")
