from flask_restful import reqparse, abort, Api, Resource
from .. import helpers
from flask_login import login_user
from flask_jwt_extended import create_access_token
import datetime
from ..database import database

parser = reqparse.RequestParser()


class OrganizationCreateAPIView(Resource):
    """ Generic API view to create organization and OrganizationOonfiguration """

    def post(self):
        parser.add_argument("abbreviatedName", type=str)
        parser.add_argument("domainName", type=str)
        parser.add_argument("environment", type=str)
        parser.add_argument("organizationName", type=str)
        parser.add_argument("region", type=dict)
        parser.add_argument("selectedPlan", type=str)
        parser.add_argument("userId", type=str)
        args = parser.parse_args()
        user = helpers.get_user_by_id(args["userId"])
        if user is not None:
            organization = helpers.create_organization(args["organizationName"])
            organization_configuration = helpers.create_organization_config(args)
            _ = helpers.create_user_org_relationships(
                user.id, organization.id, organization_configuration.id
            )
            organizations = helpers.get_user_organizations_details(user.id)
            return helpers.getValidResponse(
                helpers.get_user_details_response(organizations, user), 200
            )
        else:
            return helpers.getInvalidResponse("User with the given id does not exist !")


class OrganizationDeleteAPIView(Resource):
    """ Generic API view to delete an Organization and its corresponding OrganizationConfiguration """

    def delete(self, organization_configuration_id, user_id, organization_id):
        helpers.get_user_organization_relationship(
            organization_id, organization_configuration_id
        )
        helpers.delete_organization_by_id(organization_id)
        user = helpers.get_user_by_id(user_id)
        if user is not None:
            organizations = helpers.get_user_organizations_details(user.id)
            return helpers.getValidResponse(
                helpers.get_user_details_response(organizations, user), 200
            )
        else:
            return helpers.getInvalidResponse("User with the given id does not exist !")
