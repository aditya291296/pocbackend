from .database import *
from flask import request, flash, jsonify
from urllib.parse import urlparse, urljoin
from .database import *
from flask_login import login_user
from flask import Response
from functools import singledispatch
import json
import string
import random
from decouple import config
import uuid
import boto3


def generate_random_password():
    password_length = 10
    random_password = "".join(
        random.choices(string.ascii_lowercase + string.digits, k=password_length)
    )
    random_password = random_password.capitalize()
    return random_password


def check_if_user_exists_by_email(email_id):
    return User.query.filter_by(email=email_id).first()


def check_if_user_exists_by_mobile(mobile_number):
    return User.query.filter_by(mobile_number=mobile_number).first()


def request_json():
    best = request.accept_mimetypes.best_match(["application/json", "text/html"])
    return (
        best == "application/json"
        and request.accept_mimetypes[best] > request.accept_mimetypes["text/html"]
    )


def handle_form_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(error)
            if request_json():
                error = {"message": error, "code": 400, "type": "bad_request"}
                return jsonify({"error": error}), 400
            break
        break


def is_safe_url(target):
    ref_url = request.host_url
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc


def if_email_already_exists(email):
    return False if User.query.filter_by(email=email).first() is None else True


def create_user(params):
    user = User(
        id=str(uuid.uuid4()),
        email=params["email"],
    )
    user.set_password(params["password"])
    database.session.add(user)
    database.session.commit()
    return user


def create_user_via_gmail_response(args):
    user = User(
        email=args["email"],
        google_id=args["google_id"],
        image_url=args["image_url"],
        mobile_number="",
    )
    user.set_password(generate_random_password())
    database.session.add(user)
    database.session.commit()
    return user


def get_user_by_email(email):
    return User.query.filter_by(email=email).first()


def get_key(key):
    return CacheValue.query.filter_by(id=key).first()


def getInvalidResponse(message):
    return Response(
        message,
        status=500,
        mimetype="application/json",
    )


@singledispatch
def getValidResponse(message, status):
    raise NotImplementedError("unsupported type")


@getValidResponse.register(str)
@getValidResponse.register(int)
def _(message, status):
    return Response(message, status=status, mimetype="application/json")


@getValidResponse.register(dict)
@getValidResponse.register(int)
def _(message, status):
    return Response(json.dumps(message), status=status, mimetype="application/json")


def generate_verification_link(email):
    key = str(uuid.uuid4())
    return (
        config("BASE_URL")
        + "/verify/email/?key={key}&value={value}".format(key=key, value=email),
        key,
    )


def set_value_in_cache(uuid, email):
    # Using database for now
    # cache.set(uuid, email)
    cache_value = CacheValue(id=uuid, email=email)
    database.session.add(cache_value)
    database.session.commit()


def send_verification_email(email):
    CONFIGURATION_SET = "ConfigSet"
    SUBJECT = "Boomerang Please verify your email"
    BODY_TEXT = (
        "Welcome \r\n"
        "This email was sent with Amazon SES using the "
        "AWS SDK for Python (Boto)."
    )
    RECIPIENT = config("RECIPIENT")
    SENDER = config("SENDER")
    AWS_REGION = config("AWS_REGION")
    verification_link, cache_key = generate_verification_link(email)
    set_value_in_cache(cache_key, email)
    # The HTML body of the email.
    BODY_HTML = """<html>
    <head></head>
    <body>
    <h1>Amazon SES Test (SDK for Python)</h1>
    <p><a href={verification_link}>Verify email address </a>
        If the link doesn’t work, copy and paste the following link into your browser: 
        {verification_link}
        THIS IS AN AUTOMATED MESSAGE. PLEASE DO NOT REPLY.
        The contents of this communication, including any attachment(s), are confidential. If you are not the intended recipient (or are not receiving this communication on behalf of the intended recipient), please notify the sender immediately and delete or destroy this communication without reading it, and without making, forwarding, or retaining any copy or record of it or its contents. Thank you. Note: We have taken precautions against viruses, but take no responsibility for loss or damage caused by any virus present.</p>
    </body>
    </html>
                """.format(
        verification_link=verification_link
    )
    # The character encoding for the email.
    CHARSET = "UTF-8"
    client = boto3.client(
        "ses",
        region_name=AWS_REGION,
        aws_access_key_id=config("AWS_ACCESS_KEY"),
        aws_secret_access_key=config("AWS_SECRET_ACCESS_KEY"),
    )
    try:
        response = client.send_email(
            Destination={
                "ToAddresses": [
                    RECIPIENT,
                ],
            },
            Message={
                "Body": {
                    "Html": {
                        "Charset": CHARSET,
                        "Data": BODY_HTML,
                    },
                    "Text": {
                        "Charset": CHARSET,
                        "Data": BODY_TEXT,
                    },
                },
                "Subject": {
                    "Charset": CHARSET,
                    "Data": SUBJECT,
                },
            },
            Source=SENDER,
        )
    except ClientError as e:
        print(e.response["Error"]["Message"])
    else:
        print("Email sent! Message ID:"),
        print(response["MessageId"])


def set_user_email_confirmed_flag(user):
    fetched_user = get_user_by_email(user.email)
    fetched_user.is_email_confirmed = True
    database.session.commit()


def remove_value_from_cache(fetched_record):
    try:
        fetched_record.delete()
    except Exception as e:
        print(str(e))


def get_user_by_id(user_id):
    return User.query.filter_by(id=user_id).first()


def create_organization_configuration(args):
    organization_configuration = OrganizationConfiguration(
        id=str(uuid.uuid4()),
        domain_name=args["domain_name"],
        plan=args["selected_plan"],
    )
    database.session.add(organization_configuration)
    database.session.commit()
    return organization_configuration


def create_organization_config(args):
    organization_configuration = OrganizationConfiguration(
        id=str(uuid.uuid4()),
        domain_name=args["domainName"],
        environment=args["environment"],
        plan=args["selectedPlan"],
        abbreviated_name=args["abbreviatedName"],
        region=args["region"]["label"],
    )
    database.session.add(organization_configuration)
    database.session.commit()
    return organization_configuration


@singledispatch
def create_organization(args):
    organization = Organization(
        id=str(uuid.uuid4()), organization_name=args["organization_name"]
    )
    database.session.add(organization)
    database.session.commit()
    return organization


@create_organization.register(str)
def _(organization_name):
    organization_name = Organization(
        id=str(uuid.uuid4()), organization_name=organization_name
    )
    database.session.add(organization_name)
    database.session.commit()
    return organization_name


def create_user_org_relationships(user_id, org_id, user_org_relationship_id):
    user_org_relationship = UserOrganizationRelationship(
        id=str(uuid.uuid4()),
        user_id=user_id,
        organization_id=org_id,
        organization_configuration_id=user_org_relationship_id,
    )
    database.session.add(user_org_relationship)
    database.session.commit()
    return user_org_relationship


def get_user_organizations_details(user_id):
    user_organization_relationships = UserOrganizationRelationship.query.filter_by(
        user_id=user_id
    )
    organizations = []
    for user_organization_relationship in user_organization_relationships:
        fetched_org = Organization.query.get(
            user_organization_relationship.organization_id
        )
        organization_config = OrganizationConfiguration.query.get(
            user_organization_relationship.organization_configuration_id
        )
        organizations.append(
            {
                "id": fetched_org.id,
                "organizationName": fetched_org.organization_name,
                "organizationConfiguration": {
                    "id": organization_config.id,
                    "domainName": organization_config.domain_name,
                    "plan": organization_config.plan,
                    "numberOfUsers": organization_config.number_of_users,
                    "abbreviatedName": organization_config.abbreviated_name,
                    "environment": organization_config.environment,
                    "region": organization_config.region,
                },
            }
        )
    return organizations


def get_user_details_response(organizations, fetched_user):
    return {
        "organizations": organizations,
        "user": {
            "id": fetched_user.id,
            "email": fetched_user.email,
            "mobileNumber": fetched_user.mobile_number,
            "googleId": fetched_user.google_id,
            "imageUrl": fetched_user.image_url,
            "isEmailConfirmed": fetched_user.is_email_confirmed,
            "areOnboardingDetailsSubmitted": fetched_user.are_onboarding_details_submitted,
        },
    }


def delete_organization_by_id(organization_id):
    try:
        Organization.query.filter_by(id=organization_id).delete()
        database.session.commit()
    except Exception as e:
        print(str(e), "Error at delete_organization_by_id")


def get_user_organization_relationship(organization_id, organization_configuration_id):
    try:
        UserOrganizationRelationship.query.filter_by(
            organization_id=organization_id,
            organization_configuration_id=organization_configuration_id,
        ).delete()
        database.session.commit()
    except Exception as e:
        print(str(e), "Error at get_user_organization_relationship")


def delete_organization_configuration(organization_configuration_id):
    try:
        OrganizationConfiguration.query.filter_by(
            id=organization_configuration_id
        ).delete()
        database.session.commit()
    except Exception as e:
        print(str(e), "Error at delete_organization_configuration")
