import users_pb2_grpc as users_service
import grpc
from concurrent import futures
import time

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class UserService(users_service.UsersServicer):
    def CreateUser(self, request, context):
        metadata = dict(context.invocation_metadata())
        print(metadata, "METADATA : ======>")
        user = users_service.User(
            email=request.username,
            password=request.password,
            mobile_number=request.mobile_number,
        )
        return users_service.CreateUserResult(user=user)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    users_service.add_UsersServicer_to_server(UserService(), server)
    server.add_insecure_port("127.0.0.1:50051")
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    serve()