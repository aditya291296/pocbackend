from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from uuid import uuid4
from werkzeug.security import generate_password_hash, check_password_hash

database = SQLAlchemy()


class CacheValue(database.Model, UserMixin):
    __tablename__ = "cache_values"
    id = database.Column(database.String(50), primary_key=True, default="")
    email = database.Column(
        database.String(40), unique=True, index=True, nullable=False
    )


class OrganizationConfiguration(database.Model, UserMixin):
    __tablename__ = "organization_configurations"
    id = database.Column(database.String(50), primary_key=True, default=str(uuid4()))
    domain_name = database.Column(database.String(150), unique=True, nullable=False)
    plan = database.Column(database.String(150), unique=False, nullable=False)
    number_of_users = database.Column(
        database.Integer, unique=False, nullable=False, default=0
    )
    abbreviated_name = database.Column(
        database.String(150), unique=False, nullable=True
    )
    environment = database.Column(database.String(150), unique=False, nullable=True)
    region = database.Column(database.String(150), unique=False, nullable=True)


class Organization(database.Model, UserMixin):
    __tablename__ = "organizations"
    id = database.Column(database.String(50), primary_key=True, default=str(uuid4()))
    organization_name = database.Column(
        database.String(150), unique=True, nullable=False
    )


class User(database.Model, UserMixin):
    __tablename__ = "users"
    id = database.Column(database.String(50), primary_key=True, default=str(uuid4()))
    email = database.Column(
        database.String(40), unique=True, index=True, nullable=False
    )
    password = database.Column(database.String(150), unique=False, nullable=False)
    mobile_number = database.Column(database.String(20), unique=True, nullable=True)
    organization_id = database.Column(
        database.String(150),
        database.ForeignKey(Organization.id),
    )
    google_id = database.Column(database.String(150), unique=True, nullable=True)
    image_url = database.Column(database.String(150), unique=True, nullable=True)
    is_email_confirmed = database.Column(
        database.Boolean(), nullable=True, default=False
    )
    are_onboarding_details_submitted = database.Column(
        database.Boolean(), nullable=True, default=False
    )

    organization_configuration_id = database.Column(
        database.String(150),
        database.ForeignKey(OrganizationConfiguration.id),
    )
    organization_configuration = database.relationship(
        "OrganizationConfiguration",
        foreign_keys="User.organization_configuration_id",
    )
    organization = database.relationship(
        "Organization", foreign_keys="User.organization_id"
    )

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)


class UserOrganizationRelationship(database.Model):
    __tablename__ = "user_organization_relationships"
    id = database.Column(database.String(50), primary_key=True, default=str(uuid4()))
    user_id = database.Column(
        database.String(150),
        database.ForeignKey(User.id),
    )
    user = database.relationship(
        "User",
        foreign_keys="UserOrganizationRelationship.user_id",
    )
    organization_id = database.Column(
        database.String(150),
        database.ForeignKey(Organization.id),
    )
    organization = database.relationship(
        "Organization",
        foreign_keys="UserOrganizationRelationship.organization_id",
    )
    organization_configuration_id = database.Column(
        database.String(150),
        database.ForeignKey(OrganizationConfiguration.id),
    )
    organization_configuration = database.relationship(
        "OrganizationConfiguration",
        foreign_keys="UserOrganizationRelationship.organization_configuration_id",
    )